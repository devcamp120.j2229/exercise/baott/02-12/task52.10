import com.devcamp.j03.javabasic.s10.Person;
import java.util.ArrayList;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Person> arrayList = new ArrayList<>();
        // Khởi tạo person với các tham số khác nhau
        Person person0 = new Person("");
        Person person1 = new Person("Name", 30, 68.8, 5000000, new String[] { "Little Camel" });
        arrayList.add(person0);
        arrayList.add(person1);
        // In ra màn hình
        for (Person person : arrayList) {
            System.out.println(person);
        }

    }
}
